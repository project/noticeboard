# Noticeboard

## Entities

### Notice entity

This will be used to create notices. A notice entity has the following fields:

 - Notice message
   - WYSIWYG editor. This will be the message that the alert shows.
 - Active Range
   - A start date and time and an end date and time. Used to determine if the alert is be visible.

### Notice Board Block

This will display the current active notices.
There will be a setting on the block config form to choose how many notices to display, by default it displayes all.
