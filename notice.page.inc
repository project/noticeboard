<?php

/**
 * @file
 * Contains notice.page.inc.
 *
 * Page callback for Notice entities.
 */

use Drupal\Core\Render\Element;

/**
 * Prepares variables for Notice templates.
 *
 * Default template: notice.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_notice(array &$variables) {
  // Fetch Notice Entity Object.
  $notice = $variables['elements']['#notice'];

  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}
