<?php

namespace Drupal\noticeboard\Controller;

use Drupal\Component\Utility\Xss;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Url;
use Drupal\noticeboard\Entity\NoticeInterface;

/**
 * Class NoticeController.
 *
 *  Returns responses for Notice routes.
 *
 * @package Drupal\noticeboard\Controller
 */
class NoticeController extends ControllerBase implements ContainerInjectionInterface {

  /**
   * Displays a Notice  revision.
   *
   * @param int $notice_revision
   *   The Notice  revision ID.
   *
   * @return array
   *   An array suitable for drupal_render().
   */
  public function revisionShow($notice_revision) {
    $notice = $this->entityManager()->getStorage('notice')->loadRevision($notice_revision);
    $view_builder = $this->entityManager()->getViewBuilder('notice');

    return $view_builder->view($notice);
  }

  /**
   * Page title callback for a Notice  revision.
   *
   * @param int $notice_revision
   *   The Notice  revision ID.
   *
   * @return string
   *   The page title.
   */
  public function revisionPageTitle($notice_revision) {
    $notice = $this->entityManager()->getStorage('notice')->loadRevision($notice_revision);
    return $this->t('Revision of %title from %date', ['%title' => $notice->label(), '%date' => format_date($notice->getRevisionCreationTime())]);
  }

  /**
   * Generates an overview table of older revisions of a Notice .
   *
   * @param \Drupal\noticeboard\Entity\NoticeInterface $notice
   *   A Notice  object.
   *
   * @return array
   *   An array as expected by drupal_render().
   */
  public function revisionOverview(NoticeInterface $notice) {
    $account = $this->currentUser();
    $langcode = $notice->language()->getId();
    $langname = $notice->language()->getName();
    $languages = $notice->getTranslationLanguages();
    $has_translations = (count($languages) > 1);
    $notice_storage = $this->entityManager()->getStorage('notice');

    $build['#title'] = $has_translations ? $this->t('@langname revisions for %title', ['@langname' => $langname, '%title' => $notice->label()]) : $this->t('Revisions for %title', ['%title' => $notice->label()]);
    $header = [$this->t('Revision'), $this->t('Operations')];

    $revert_permission = (($account->hasPermission("revert all notice revisions") || $account->hasPermission('administer notice entities')));
    $delete_permission = (($account->hasPermission("delete all notice revisions") || $account->hasPermission('administer notice entities')));

    $rows = [];

    $vids = $notice_storage->revisionIds($notice);

    $latest_revision = TRUE;

    foreach (array_reverse($vids) as $vid) {
      /** @var \Drupal\noticeboard\NoticeInterface $revision */
      $revision = $notice_storage->loadRevision($vid);
      // Only show revisions that are affected by the language that is being
      // displayed.
      if ($revision->hasTranslation($langcode) && $revision->getTranslation($langcode)->isRevisionTranslationAffected()) {
        $username = [
          '#theme' => 'username',
          '#account' => $revision->getRevisionUser(),
        ];

        // Use revision link to link to revisions that are not active.
        $date = \Drupal::service('date.formatter')->format($revision->getRevisionCreationTime(), 'short');
        if ($vid != $notice->getRevisionId()) {
          $link = $this->l($date, new Url('entity.notice.revision', ['notice' => $notice->id(), 'notice_revision' => $vid]));
        }
        else {
          $link = $notice->link($date);
        }

        $row = [];
        $column = [
          'data' => [
            '#type' => 'inline_template',
            '#template' => '{% trans %}{{ date }} by {{ username }}{% endtrans %}{% if message %}<p class="revision-log">{{ message }}</p>{% endif %}',
            '#context' => [
              'date' => $link,
              'username' => \Drupal::service('renderer')->renderPlain($username),
              'message' => ['#markup' => $revision->getRevisionLogMessage(), '#allowed_tags' => Xss::getHtmlTagList()],
            ],
          ],
        ];
        $row[] = $column;

        if ($latest_revision) {
          $row[] = [
            'data' => [
              '#prefix' => '<em>',
              '#markup' => $this->t('Current revision'),
              '#suffix' => '</em>',
            ],
          ];
          foreach ($row as &$current) {
            $current['class'] = ['revision-current'];
          }
          $latest_revision = FALSE;
        }
        else {
          $links = [];
          if ($revert_permission) {
            $links['revert'] = [
              'title' => $this->t('Revert'),
              'url' => $has_translations ?
              Url::fromRoute('entity.notice.translation_revert', ['notice' => $notice->id(), 'notice_revision' => $vid, 'langcode' => $langcode]) :
              Url::fromRoute('entity.notice.revision_revert', ['notice' => $notice->id(), 'notice_revision' => $vid]),
            ];
          }

          if ($delete_permission) {
            $links['delete'] = [
              'title' => $this->t('Delete'),
              'url' => Url::fromRoute('entity.notice.revision_delete', ['notice' => $notice->id(), 'notice_revision' => $vid]),
            ];
          }

          $row[] = [
            'data' => [
              '#type' => 'operations',
              '#links' => $links,
            ],
          ];
        }

        $rows[] = $row;
      }
    }

    $build['notice_revisions_table'] = [
      '#theme' => 'table',
      '#rows' => $rows,
      '#header' => $header,
    ];

    return $build;
  }

}
