<?php

namespace Drupal\noticeboard\Entity;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\RevisionableContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\user\UserInterface;

/**
 * Defines the Notice entity.
 *
 * @ingroup noticeboard
 *
 * @ContentEntityType(
 *   id = "notice",
 *   label = @Translation("Notice"),
 *   handlers = {
 *     "storage" = "Drupal\noticeboard\NoticeStorage",
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\noticeboard\NoticeListBuilder",
 *     "views_data" = "Drupal\noticeboard\Entity\NoticeViewsData",
 *     "translation" = "Drupal\noticeboard\NoticeTranslationHandler",
 *
 *     "form" = {
 *       "default" = "Drupal\noticeboard\Form\NoticeForm",
 *       "add" = "Drupal\noticeboard\Form\NoticeForm",
 *       "edit" = "Drupal\noticeboard\Form\NoticeForm",
 *       "delete" = "Drupal\noticeboard\Form\NoticeDeleteForm",
 *     },
 *     "access" = "Drupal\noticeboard\NoticeAccessControlHandler",
 *     "route_provider" = {
 *       "html" = "Drupal\noticeboard\NoticeHtmlRouteProvider",
 *     },
 *   },
 *   base_table = "notice",
 *   data_table = "notice_field_data",
 *   revision_table = "notice_revision",
 *   revision_data_table = "notice_field_revision",
 *   translatable = TRUE,
 *   admin_permission = "administer notice entities",
 *   entity_keys = {
 *     "id" = "id",
 *     "revision" = "vid",
 *     "label" = "name",
 *     "uuid" = "uuid",
 *     "uid" = "user_id",
 *     "langcode" = "langcode",
 *     "status" = "status",
 *   },
 *   links = {
 *     "canonical" = "/admin/structure/notice/{notice}",
 *     "add-form" = "/admin/structure/notice/add",
 *     "edit-form" = "/admin/structure/notice/{notice}/edit",
 *     "delete-form" = "/admin/structure/notice/{notice}/delete",
 *     "version-history" = "/admin/structure/notice/{notice}/revisions",
 *     "revision" = "/admin/structure/notice/{notice}/revisions/{notice_revision}/view",
 *     "revision_revert" = "/admin/structure/notice/{notice}/revisions/{notice_revision}/revert",
 *     "translation_revert" = "/admin/structure/notice/{notice}/revisions/{notice_revision}/revert/{langcode}",
 *     "revision_delete" = "/admin/structure/notice/{notice}/revisions/{notice_revision}/delete",
 *     "collection" = "/admin/structure/notice",
 *   },
 *   field_ui_base_route = "notice.settings"
 * )
 */
class Notice extends RevisionableContentEntityBase implements NoticeInterface {

  use EntityChangedTrait;

  /**
   * {@inheritdoc}
   */
  public static function preCreate(EntityStorageInterface $storage_controller, array &$values) {
    parent::preCreate($storage_controller, $values);
    $values += [
      'user_id' => \Drupal::currentUser()->id(),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function preSave(EntityStorageInterface $storage) {
    parent::preSave($storage);

    foreach (array_keys($this->getTranslationLanguages()) as $langcode) {
      $translation = $this->getTranslation($langcode);

      // If no owner has been set explicitly, make the anonymous user the owner.
      if (!$translation->getOwner()) {
        $translation->setOwnerId(0);
      }
    }

    // If no revision author has been set explicitly, make the notice owner the
    // revision author.
    if (!$this->getRevisionUser()) {
      $this->setRevisionUserId($this->getOwnerId());
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getName() {
    return $this->get('name')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setName($name) {
    $this->set('name', $name);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwner() {
    return $this->get('user_id')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwnerId() {
    return $this->get('user_id')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwnerId($uid) {
    $this->set('user_id', $uid);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwner(UserInterface $account) {
    $this->set('user_id', $account->id());
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function isPublished() {
    return (bool) $this->getEntityKey('status');
  }

  /**
   * {@inheritdoc}
   */
  public function setPublished($published) {
    $this->set('status', $published ? TRUE : FALSE);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['user_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Authored by'))
      ->setDescription(t('The user ID of author of the Notice entity.'))
      ->setRevisionable(TRUE)
      ->setSetting('target_type', 'user')
      ->setSetting('handler', 'default')
      ->setTranslatable(TRUE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'author',
        'weight' => 0,
      ])
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'weight' => 5,
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => '60',
          'autocomplete_type' => 'tags',
          'placeholder' => '',
        ],
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Name'))
      ->setDescription(t('The name of the Notice entity.'))
      ->setRevisionable(TRUE)
      ->setSettings([
        'max_length' => 50,
        'text_processing' => 0,
      ])
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => 1,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => 1,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['active_range'] = BaseFieldDefinition::create('daterange')
      ->setLabel(t('Active range'))
      ->setDescription(t('The 2 dates that the notice will appear between.'))
      ->setRevisionable(TRUE)
      ->setRequired(TRUE)
      ->setDefaultValue([
        'default_date_type' => 'now',
        'default_date' => 'now',
        'default_end_date_type' => 'relative',
        'default_end_date' => '+1 day',
      ])
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => 2,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => 2,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['notice_message'] = BaseFieldDefinition::create('text_long')
      ->setLabel(t('Notice message'))
      ->setDescription(t('The message for your notice.'))
      ->setRevisionable(TRUE)
      ->setRequired(TRUE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'text_default',
        'weight' => 3,
      ])
      ->setDisplayOptions('form', [
        'type' => 'text_textfield',
        'weight' => 3,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['status'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Publishing status'))
      ->setDescription(t('A boolean indicating whether the Notice is published.'))
      ->setRevisionable(TRUE)
      ->setDefaultValue(TRUE);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time that the entity was created.'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the entity was last edited.'));

    $fields['revision_translation_affected'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Revision translation affected'))
      ->setDescription(t('Indicates if the last edit of a translation belongs to current revision.'))
      ->setReadOnly(TRUE)
      ->setRevisionable(TRUE)
      ->setTranslatable(TRUE);

    return $fields;
  }

  /**
   * Checks if the notice is currently active.
   *
   * @return bool
   */
  public function isActive() : bool {
    $active = FALSE;

    // TODO: Check the dates and determine if the notice is active.

    return $active;
  }

}
