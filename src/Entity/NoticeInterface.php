<?php

namespace Drupal\noticeboard\Entity;

use Drupal\Core\Entity\RevisionLogInterface;
use Drupal\Core\Entity\RevisionableInterface;
use Drupal\Component\Utility\Xss;
use Drupal\Core\Url;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining Notice entities.
 *
 * @ingroup noticeboard
 */
interface NoticeInterface extends RevisionableInterface, RevisionLogInterface, EntityChangedInterface, EntityOwnerInterface {

  // Add get/set methods for your configuration properties here.

  /**
   * Gets the Notice name.
   *
   * @return string
   *   Name of the Notice.
   */
  public function getName();

  /**
   * Sets the Notice name.
   *
   * @param string $name
   *   The Notice name.
   *
   * @return \Drupal\noticeboard\Entity\NoticeInterface
   *   The called Notice entity.
   */
  public function setName($name);

  /**
   * Gets the Notice creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Notice.
   */
  public function getCreatedTime();

  /**
   * Sets the Notice creation timestamp.
   *
   * @param int $timestamp
   *   The Notice creation timestamp.
   *
   * @return \Drupal\noticeboard\Entity\NoticeInterface
   *   The called Notice entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Returns the Notice published status indicator.
   *
   * Unpublished Notice are only visible to restricted users.
   *
   * @return bool
   *   TRUE if the Notice is published.
   */
  public function isPublished();

  /**
   * Sets the published status of a Notice.
   *
   * @param bool $published
   *   TRUE to set this Notice to published, FALSE to set it to unpublished.
   *
   * @return \Drupal\noticeboard\Entity\NoticeInterface
   *   The called Notice entity.
   */
  public function setPublished($published);

  /**
   * Gets the Notice revision creation timestamp.
   *
   * @return int
   *   The UNIX timestamp of when this revision was created.
   */
  public function getRevisionCreationTime();

  /**
   * Sets the Notice revision creation timestamp.
   *
   * @param int $timestamp
   *   The UNIX timestamp of when this revision was created.
   *
   * @return \Drupal\noticeboard\Entity\NoticeInterface
   *   The called Notice entity.
   */
  public function setRevisionCreationTime($timestamp);

  /**
   * Gets the Notice revision author.
   *
   * @return \Drupal\user\UserInterface
   *   The user entity for the revision author.
   */
  public function getRevisionUser();

  /**
   * Sets the Notice revision author.
   *
   * @param int $uid
   *   The user ID of the revision author.
   *
   * @return \Drupal\noticeboard\Entity\NoticeInterface
   *   The called Notice entity.
   */
  public function setRevisionUserId($uid);

}
