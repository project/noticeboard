<?php

namespace Drupal\noticeboard\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Notice entities.
 *
 * @ingroup noticeboard
 */
class NoticeDeleteForm extends ContentEntityDeleteForm {


}
