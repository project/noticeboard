<?php

namespace Drupal\noticeboard;

use Drupal\Core\Entity\Sql\SqlContentEntityStorage;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\noticeboard\Entity\NoticeInterface;

/**
 * Defines the storage handler class for Notice entities.
 *
 * This extends the base storage class, adding required special handling for
 * Notice entities.
 *
 * @ingroup noticeboard
 */
class NoticeStorage extends SqlContentEntityStorage implements NoticeStorageInterface {

  /**
   * {@inheritdoc}
   */
  public function revisionIds(NoticeInterface $entity) {
    return $this->database->query(
      'SELECT vid FROM {notice_revision} WHERE id=:id ORDER BY vid',
      [':id' => $entity->id()]
    )->fetchCol();
  }

  /**
   * {@inheritdoc}
   */
  public function userRevisionIds(AccountInterface $account) {
    return $this->database->query(
      'SELECT vid FROM {notice_field_revision} WHERE uid = :uid ORDER BY vid',
      [':uid' => $account->id()]
    )->fetchCol();
  }

  /**
   * {@inheritdoc}
   */
  public function countDefaultLanguageRevisions(NoticeInterface $entity) {
    return $this->database->query('SELECT COUNT(*) FROM {notice_field_revision} WHERE id = :id AND default_langcode = 1', [':id' => $entity->id()])
      ->fetchField();
  }

  /**
   * {@inheritdoc}
   */
  public function clearRevisionsLanguage(LanguageInterface $language) {
    return $this->database->update('notice_revision')
      ->fields(['langcode' => LanguageInterface::LANGCODE_NOT_SPECIFIED])
      ->condition('langcode', $language->getId())
      ->execute();
  }

}
