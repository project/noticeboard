<?php

namespace Drupal\noticeboard;

use Drupal\Core\Entity\ContentEntityStorageInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\noticeboard\Entity\NoticeInterface;

/**
 * Defines the storage handler class for Notice entities.
 *
 * This extends the base storage class, adding required special handling for
 * Notice entities.
 *
 * @ingroup noticeboard
 */
interface NoticeStorageInterface extends ContentEntityStorageInterface {

  /**
   * Gets a list of Notice revision IDs for a specific Notice.
   *
   * @param \Drupal\noticeboard\Entity\NoticeInterface $entity
   *   The Notice entity.
   *
   * @return int[]
   *   Notice revision IDs (in ascending order).
   */
  public function revisionIds(NoticeInterface $entity);

  /**
   * Gets a list of revision IDs having a given user as Notice author.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The user entity.
   *
   * @return int[]
   *   Notice revision IDs (in ascending order).
   */
  public function userRevisionIds(AccountInterface $account);

  /**
   * Counts the number of revisions in the default language.
   *
   * @param \Drupal\noticeboard\Entity\NoticeInterface $entity
   *   The Notice entity.
   *
   * @return int
   *   The number of revisions in the default language.
   */
  public function countDefaultLanguageRevisions(NoticeInterface $entity);

  /**
   * Unsets the language for all Notice with the given language.
   *
   * @param \Drupal\Core\Language\LanguageInterface $language
   *   The language object.
   */
  public function clearRevisionsLanguage(LanguageInterface $language);

}
