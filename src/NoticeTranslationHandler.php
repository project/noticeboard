<?php

namespace Drupal\noticeboard;

use Drupal\content_translation\ContentTranslationHandler;

/**
 * Defines the translation handler for notice.
 */
class NoticeTranslationHandler extends ContentTranslationHandler {

  // Override here the needed methods from ContentTranslationHandler.

}
