<?php

namespace Drupal\noticeboard\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Entity\EntityTypeManager;
use Drupal\Core\Entity\EntityViewBuilderInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\noticeboard\Entity\Notice;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a 'Noticeboard' block.
 *
 * @Block(
 *  id = "noticeboard",
 *  admin_label = @Translation("Noticeboard"),
 * )
 */
class Noticeboard extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * Drupal\Core\Entity\EntityTypeManager definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManager
   */
  protected $entityTypeManager;

  /**
   * Constructs a new Noticeboard object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param string $plugin_definition
   *   The plugin implementation definition.
   * @param EntityTypeManager $entity_type_manager
   */
  public function __construct(
        array $configuration,
        $plugin_id,
        $plugin_definition,
        EntityTypeManager $entity_type_manager
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
         'number_of_notices' => -1,
        ] + parent::defaultConfiguration();

 }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form['number_of_notices'] = [
      '#type' => 'number',
      '#title' => $this->t('Number of notices'),
      '#description' => $this->t('The max number of notices to display at one time. (Set to -1 for unlimited)'),
      '#default_value' => $this->configuration['number_of_notices'],
      '#weight' => '0',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $this->configuration['number_of_notices'] = $form_state->getValue('number_of_notices');
  }

  /**
   * {@inheritdoc}
   * TODO: Change this to use an entity field query with a limit.
   */
  public function build() {
    $build = [];

    // TODO: Disable block cache.

    /**
     * The number of notices we are going to render.
     *
     * @var integer $number_to_render
     */
    $number_to_render = $this->configuration['number_of_notices'];

    // The number of notices that have been rendered.
    $number_rendered = 0;

    /** @var \Drupal\noticeboard\Entity\NoticeInterface[] $notices */
    $notices = Notice::loadMultiple();

    foreach ($notices as $notice) {
      if ($number_to_render == -1 || $number_rendered < $number_to_render) {
        if ($notice->isActive()) {
          $number_rendered++;

          /** @var \Drupal\Core\Entity\EntityViewBuilderInterface $notice_view_builder */
          $notice_view_builder = $this->entityTypeManager->getViewBuilder('notice');
          $build['notices'][$notice->id()] = $notice_view_builder->view($notice);
        }
      }
    }

    return $build;
  }

}
